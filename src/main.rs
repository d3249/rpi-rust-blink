use rppal::gpio::Gpio;
use std::{thread, time};

fn main()  {

    let mut led = Gpio::new().expect("Can't create Gpio instance").get(2).expect("Can't get pin").into_output();

    let mut counter = 0;
    let limit = 100;

    loop {

        led.toggle();

        counter += 1;

        if counter == limit {
            led.set_low();
            break ;
        }

        thread::sleep(time::Duration::from_millis(500));
    }



}
